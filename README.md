# StopOffice

Quickly made to kill off the jodservice on development machines via a gui (it was taking a bit long to open up terminal each time to run "killall soffice.bin" each time).

## Version History
1.2 - Made application minimize under any circumstance, now will show image regardless of where it was executed, removed label - now outputs to stdin

1.1.1 - Replaced text with stop icon and big mahoosive button (01/05/2012)

1.1 - Supports Windows 

1.0 - Made today, works \o/ (28/04/2012)
