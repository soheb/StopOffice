#!/usr/bin/python
'''
Created on 28 Apr 2012

@author: soheb
'''

import sys, os, datetime
from PyQt4 import QtGui, QtCore
from subprocess import call

class Example(QtGui.QWidget):
    def __init__(self):
        super(Example,self).__init__()
        self.initUI()
        
    def initUI(self):
        imagepath = sys.path[0] + '/' + 'stop.svg'
        QtGui.QToolTip.setFont(QtGui.QFont('DeJaVu Sans Mono', 10))        
        btn = QtGui.QPushButton('', self)
        btn.setToolTip('Press this to kill OpenOffice server process')
        btn.resize(180,180)
        btn.move(10,10)
        btn.clicked.connect(self.killoffice)
        buttonImage = QtGui.QPixmap(imagepath)
        btn.setIcon(QtGui.QIcon(buttonImage))
        btn.setIconSize(QtCore.QSize(150,150))
        self.resize(200,200)
        self.center()
        
        self.setWindowTitle('Stop Office')
        self.setWindowIcon(QtGui.QIcon(imagepath))
        self.show()
        
    def killoffice(self):
        if os.name=="nt":
            try:
                call(["taskkil","/IM","soffice.exe"])
                print str(datetime.datetime.now()) + ' Success'
            except WindowsError:
                print str(datetime.datetime.now()) + ' Failure to kill Windows Executable'
            finally:
                self.window().showMinimized();
        else:
            ret = call(["killall", "soffice.bin"])
            if ret == 0:
                print str(datetime.datetime.now()) + ' Success'
            else:
                print str(datetime.datetime.now()) + ' Failure to kill process: does not exist'
            self.window().showMinimized() 
    
    def center(self):
        qr = self.frameGeometry()
        cp = QtGui.QDesktopWidget().availableGeometry().center()
        qr.moveCenter(cp)
        self.move(qr.topLeft())          

def main():
    app = QtGui.QApplication(sys.argv)
    ex = Example()
    print sys.path[0] + "/stop.svg"
    sys.exit(app.exec_());
    
if __name__ == '__main__':
    main()
